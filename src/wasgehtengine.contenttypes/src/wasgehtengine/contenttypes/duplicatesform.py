from five import grok
from plone.app.layout.navigation.interfaces import INavigationRoot
from collective.transmogrifier.transmogrifier import Transmogrifier
from zope.site.hooks import getSite
from Products.CMFCore.utils import getToolByName
from Products import AdvancedQuery
from datetime import timedelta
import logging

class DuplicatesForm(grok.View):

	grok.context(INavigationRoot)
	grok.name('duplicates')
	grok.require('wasgehtengine.ManageDuplicates')
	
	logger = logging.getLogger('duplicates')

	def update(self):
		pass

	def __call__(self):

		form = self.request.form
	
		findDuplicatesButton = form.get('form.button.find-duplicates') is not None
		
		folder = form.get('folder')

		if(self.request.get('REQUEST_METHOD', 'GET').upper() == 'POST'):
			transmogrifier = Transmogrifier(getSite())

			if findDuplicatesButton:
				catalog = getToolByName(self.context, 'portal_catalog')
				
				query = AdvancedQuery.Eq("path", folder) & (AdvancedQuery.Eq("portal_type", "wasgehtengine.Event") | AdvancedQuery.Eq("portal_type", "wasgehtengine.Screening"))
				
        		#results = catalog.searchResults(path=dict(query=folder, depth=1), portal_type='wasgehtengine.Event')
        		results = catalog.evalAdvancedQuery(query)
        		
        		self.logger.info('Searching duplicates of ' + str(len(results)) + ' events.')
        		
        		for brain in results:
        			event = brain.getObject()
        			
        			startdate = event.start
        			
        			duplicates = catalog.searchResults(path=folder, title=event.title, period_start={ "query": [startdate-timedelta(hours=1), startdate+timedelta(hours=1)], "range": "minmax" },venue=event.venue.to_path)
        			
        			for dup_brain in duplicates:
        				if dup_brain.getPath() != brain.getPath():
        					self.logger.info('For ' + unicode(event.title) + '. ' + brain.getObject().absolute_url())
        			  		self.logger.info('Found duplicate ' + unicode(dup_brain.getObject().title) + '. ' + dup_brain.getObject().absolute_url())
        
        		

		return super(DuplicatesForm, self).__call__()
